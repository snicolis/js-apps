function getData(){
  var data = [];
  data[0] = document.getElementById('rmu').value;
  data[1] = document.getElementById('rsigma').value;
  console.log(data); // Check the console for the values for every click.
  return data;
}

function linspace(a,b,n) {
    if(typeof n === "undefined") n = Math.max(Math.round(b-a)+1,1);
    if(n<2) { return n===1?[a]:[]; }
    var i,ret = Array(n);
    n--;
    for(i=n;i>=0;i--) { ret[i] = (i*b+(n-i)*a)/n; }
    return ret;
}


function plot(){
  var xd = getData();
  var x1 = [];
  var y1 = [];
  var i ;
  mu = parseFloat(xd[0]);
  sigma = parseFloat(xd[1]);
  bb = linspace(-5*(10+mu), 5*(mu+10), 10000)
  for (i = 0; i <= 10000; i++){
    x1[i] = bb[i];
    y1[i] =  1. / (sigma * Math.sqrt(2. * Math.PI)) * Math.exp(-0.5 * ((x1[i] - mu)/sigma)**2.);
  }
  Plotly.purge('myDiv');
  var f1 = {
    x: x1,
    y: y1,
    type: 'scatter'
    };
  var data = [f1];
  var layout = {
    title: {
        text:'Gaussian distribution',
        font: {size: 18}
    },
  };

  var config = {responsive: true}

  Plotly.newPlot('myDiv', data, layout, config);
}


